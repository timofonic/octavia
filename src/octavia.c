/**
 * Octavia core functions.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <nettle/sha.h>
#include <nettle/hmac.h>

#include "octavia.h"


unsigned char * hash(unsigned char * data, size_t length, unsigned char digest [HashSize]) {
      struct sha512_ctx ctx;
      sha512_init(&ctx);
      sha512_update(&ctx, length, data);
      sha512_digest(&ctx, HashSize, digest);

      sha512_init(&ctx);
      sha512_update(&ctx, HashSize, digest);
      sha512_digest(&ctx, HashSize, digest);
      return digest;
}


unsigned char * sign(unsigned char * data, size_t length, unsigned char digest [HashSize],
                     unsigned char key [HashSize])
{
      struct hmac_sha512_ctx ctx;
      hmac_sha512_set_key(&ctx, HashSize, key);
      hmac_sha512_update(&ctx, length, data);
      hmac_sha512_digest(&ctx, HashSize, digest);
      return digest;
}


unsigned int ciphertext_size(unsigned int plaintext_size) {
      return plaintext_size / CipherBlockSize +
             (0 == plaintext_size % CipherBlockSize ? 0 : 1) * CipherBlockSize +
                   CipherBlockSize;
}


Boolean exists(const char * path) {
      (void) path;

      /*
       * TODO: Retrieve the descriptors of each directory path component,
       * and then check for the presence of the basename in the final
       * descriptor.
       */

      abort();
}


Boolean is_file(const char * path) {
      (void) path;

      /*
       * TODO: Retrieve the descriptors of each directory path component,
       * and then check for the presence of the basename in the final
       * descriptor. If present and is a file, return True.
       */

      abort();
}


Boolean is_directory(const char * path) {
      (void) path;

      /*
       * TODO: Retrieve the descriptors of each directory path component,
       * and then check for the presence of the basename in the final
       * descriptor. If present and is a directory, return True.
       */

      abort();
}

